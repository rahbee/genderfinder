package com.pocketbook.gender;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DetectGender2 {

	public static void main(String args[]) {

		List<String> maleNames = readTextFile("d://malenames1.txt");
		List<String> femaleNames = readTextFile("d://femalenames1.txt");

		getMatchWithFirstName("d://query_result.csv", maleNames,femaleNames);
		getMatchWithEmail("d://query_result.csv", maleNames, femaleNames);

	}

	private static void getMatchWithFirstName(String fileName, List<String> maleNames, List<String> femaleNames) {
		// read file into stream, try-with-resources
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = br.readLine();
			String[] emailNamePair;

			int i = 0;
			int maleCount = 0;
			int femaleCount = 0;
			double hitrate = 0.0;
			while (line != null) {
				if (i != 0) {
					emailNamePair = line.split(",");
					String firstName = emailNamePair[1].substring(1, emailNamePair[1].length() - 1).trim()
							.toLowerCase();

					boolean found = false;
					for (String str : maleNames) {
						if (str.trim().equals(firstName)) {
							System.out.println(firstName + " >> male");
							maleCount++;
							found = true;
							break;
						}
					}

					if (!found) {
						for (String str : femaleNames) {
							if (str.trim().equals(firstName)) {
								 System.out.println(firstName + " >>>> female");
								femaleCount++;
								found = true;
								break;
							}
						}
					}
					if (!found) {
						//System.out.println(firstName + "::" + emailNamePair[0]);
					}

				} else {
					// First line of the CSV HEADER

				}
				line = br.readLine();
				i++;
			}

			System.out.println();
			System.out.println();
			System.out.println("femaleCount: " + femaleCount);
			System.out.println("maleCount: " + maleCount);
			System.out.println("Total Count: " + i);
			System.out.print("Hit rate: ");
			hitrate = (maleCount + femaleCount) * 100 / i;
			System.out.print(hitrate);
			
			System.out.println();
			System.out.println();
			System.out.println("=======================================================");
			System.out.println();
			System.out.println();
			

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void getMatchWithEmail(String fileName, List<String> maleNames, List<String> femaleNames) {
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = br.readLine();
			String[] emailNamePair;

			int i = 0;
			int maleCount = 0;
			int femaleCount = 0;
			double hitrate = 0.0;
			while (line != null) {
				if (i != 0) {
					emailNamePair = line.split(",");
					String partOfEmail = emailNamePair[0].substring(1, emailNamePair[0].length() - 1).trim()
							.toLowerCase().split("@")[0];
					
					boolean found = false;
					for (String str : maleNames) {
						if ((partOfEmail.startsWith(str.trim()) || partOfEmail.endsWith(str.trim())) && str.trim().length() > 2) {
							System.out.println(str.trim() + ":  " + partOfEmail + " >> male");
							maleCount++;
							found = true;
							break;
						}
					}

					if (!found) {
						for (String str : femaleNames) {
							if ((partOfEmail.startsWith(str.trim()) || partOfEmail.endsWith(str.trim())) && str.trim().length() > 2) {
								System.out.println(str.trim() + ":  " + partOfEmail + " >>>> female");
								femaleCount++;
								found = true;
								break;
							}
						}
					}
					if (!found) {
						// System.out.println(partOfEmail +"::"+
					}

				} else {
					// First line of the CSV HEADER

				}
				line = br.readLine();
				i++;
			}

			System.out.println();
			System.out.println();
			System.out.println("femaleCount: " + femaleCount);
			System.out.println("maleCount: " + maleCount);
			System.out.println("Total Count: " + i);
			System.out.print("Hit rate: ");
			hitrate = (maleCount + femaleCount) * 100 / i;
			System.out.print(hitrate);
			
			System.out.println();
			System.out.println();
			System.out.println("=======================================================");
			
			System.out.println();
			System.out.println();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static List<String> readTextFile(String fileName) {
		List<String> listNames = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = br.readLine();

			int i = 0;

			while (line != null) {
				listNames.add(line.trim().toLowerCase());
				line = br.readLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return listNames;
	}


}
