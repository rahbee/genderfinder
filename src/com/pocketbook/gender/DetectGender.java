package com.pocketbook.gender;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DetectGender {
	
	static int i = 0;
	static int maleCount = 0;
	static int femaleCount = 0;
	static double hitrate = 0.0;

	public static void main(String args[]) {

		List<String> maleNames = readTextFile("d://malenames1.txt");
		List<String> femaleNames = readTextFile("d://femalenames1.txt");

		List<String> notFoundWithFirstNames = getMatchWithFirstName("d://query_result.csv", maleNames,femaleNames);
		List<String> stillNotFound = getMatchWithEmail(notFoundWithFirstNames, maleNames, femaleNames);
		
		System.out.println();
		System.out.println();
		System.out.println("femaleCount: " + femaleCount);
		System.out.println("maleCount: " + maleCount);
		System.out.println("Total Count: " + i);
		System.out.print("Hit rate: ");
		hitrate = (maleCount + femaleCount) * 100 / i;
		System.out.print(hitrate);
		
		System.out.println();
		System.out.println();
		System.out.println("=======================================================");
		System.out.println();
		System.out.println();
		
		// Print the list not found
		for (String str : stillNotFound) {
			System.out.println("stillNotFound $$ " + str);
		}
	}

	private static List<String> getMatchWithFirstName(String fileName, List<String> maleNames, List<String> femaleNames) {
		// read file into stream, try-with-resources
		List<String> notFoundWithFirstNames = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = br.readLine();
			String[] emailNamePair;

			i=0;
			while (line != null) {
				if (i != 0) {
					emailNamePair = line.split(",");
					String firstName = emailNamePair[1].substring(1, emailNamePair[1].length() - 1).trim()
							.toLowerCase();

					boolean found = false;
					for (String str : maleNames) {
						if (str.trim().equals(firstName)) {
							System.out.println(firstName + " >> male");
							maleCount++;
							found = true;
							break;
						}
					}

					if (!found) {
						for (String str : femaleNames) {
							if (str.trim().equals(firstName)) {
								 System.out.println(firstName + " >>>> female");
								femaleCount++;
								found = true;
								break;
							}
						}
					}
					if (!found) {
						//System.out.println(firstName + "::" + emailNamePair[0]);
						notFoundWithFirstNames.add(emailNamePair[0].substring(1, emailNamePair[0].length() - 1).trim()
								.toLowerCase());
					}

				} else {
					// First line of the CSV HEADER

				}
				line = br.readLine();
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return notFoundWithFirstNames;
	}
	
	
	private static List<String> getMatchWithEmail(List<String> listOfEmails, List<String> maleNames, List<String> femaleNames) {
		
		List<String> stillNotFound = new ArrayList<>();
		
		for (String email : listOfEmails) {
			boolean found = false;
			for (String str : maleNames) {
				if ((email.startsWith(str.trim()) || email.endsWith(str.trim())) && str.trim().length() > 2) {
					System.out.println(str.trim() + ":  " + email + " >> male");
					maleCount++;
					found = true;
					break;
				}
			}

			if (!found) {
				for (String str : femaleNames) {
					if ((email.startsWith(str.trim()) || email.endsWith(str.trim())) && str.trim().length() > 2) {
						System.out.println(str.trim() + ":  " + email + " >>>> female");
						femaleCount++;
						found = true;
						break;
					}
				}
			}
			if (!found) {
				stillNotFound.add(email);
			}
		}
		
		return stillNotFound;
	}

	private static List<String> readTextFile(String fileName) {
		List<String> listNames = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line = br.readLine();

			int i = 0;

			while (line != null) {
				listNames.add(line.trim().toLowerCase());
				line = br.readLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return listNames;
	}


}
